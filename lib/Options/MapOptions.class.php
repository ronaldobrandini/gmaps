<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace core\tools\gmap\options;

/**
 * Description of MapOptions
 *
 * @author ronaldo.silva
 */
class MapOptions extends \core\tools\gmap\Output{
    protected $definitions = array(
        'backgroundColor' => array(
            'type' => 'string',
            'default' => null,
            'value' => null
        ),
        'center' => array(
            'type' => 'array',
            'default' => null,
            'value' => null
        ),
        'zoom' => array(
            'type' => 'integer',
            'default' => null,
            'value' => null
        )
    );
    
    public function render(){
        $data = array();
        foreach($this->definitions as $key => $definition){
            if($definition['value'] !== NULL && $definition['default'] !== $definition['value']){
                $data[$key] = $definition['value'];
            }
        }
        $this->code = 'var mapOptionsmap = ' . json_encode($data);
        return parent::render();
    }
}
