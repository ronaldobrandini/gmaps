<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace core\tools\gmap\options;

/**
 * Description of PolylineOptions
 *
 * @author ronaldo.silva
 */
class PolylineOptions extends \core\tools\gmap\Output{
    protected $definitions = array(
        'clickable' => array(
            'type' => 'bool',
            'default' => true,
            'value' => true
        ),
        'draggable' => array(
            'type' => 'bool',
            'default' => false,
            'value' => false
        ),
        'editable' => array(
            'type' => 'bool',
            'default' => false,
            'value' => false
        ),
        'geodesic' => array(
            'type' => 'bool',
            'default' => false,
            'value' => false
        ),
        'icons' => array(
            'type' => 'array',
            'default' => null,
            'value' => null
        ),
        'map' => array(
            'type' => 'string',
            'default' => null,
            'value' => null
        ),
        'path' => array(
            'type' => 'array',
            'default' => null,
            'value' => null
        ),
        'strokeColor' => array(
            'type' => 'string',
            'default' => null,
            'value' => null
        ),
        'strokeOpacity' => array(
            'type' => 'float',
            'default' => null,
            'value' => null
        ),
        'strokeWeight' => array(
            'type' => 'integer',
            'default' => null,
            'value' => null
        ),
        'visible' => array(
            'type' => 'bool',
            'default' => true,
            'value' => true
        ),
        'zIndex' => array(
            'type' => 'integer',
            'default' => null,
            'value' => null
        )
    );
    
    public function render(){
        $data = array();
        foreach($this->definitions as $key => $definition){
            if($definition['value'] !== NULL && $definition['default'] !== $definition['value']){
                $data[$key] = $definition['value'];
            }
        }
        $this->code = 'var polyline = new google.maps.Polyline(' . json_encode($data) . ')';
        return parent::render();
    }
}
