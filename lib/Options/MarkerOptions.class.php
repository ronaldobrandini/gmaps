<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace core\tools\gmap\options;

/**
 * Description of MarkerOptions
 *
 * @author ronaldo.silva
 */
class MarkerOptions extends \core\tools\gmap\Output{
    protected $definitions = array(
        'anchorPoint' => array(
            'type' => 'array',
            'default' => null,
            'value' => null
        ),
        'animation' => array(
            'type' => 'array',
            'default' => null,
            'value' => null
        ),
        'clickable' => array(
            'type' => 'bool',
            'default' => true,
            'value' => true
        ),
        'crossOnDrag' => array(
            'type' => 'bool',
            'default' => true,
            'value' => true
        ),
        'cursor' => array(
            'type' => 'string',
            'default' => null,
            'value' => null
        ),
        'draggable' => array(
            'type' => 'bool',
            'default' => false,
            'value' => false
        ),
        'icon' => array(
            'type' => 'string',
            'default' => null,
            'value' => null
        ),
        'map' => array(
            'type' => 'string',
            'default' => null,
            'value' => null
        ),
        'opacity' => array(
            'type' => 'float',
            'default' => 1,
            'value' => 1
        ),
        'optimized' => array(
            'type' => 'bool',
            'default' => true,
            'value' => true
        ),
        'position' => array(
            'type' => 'array',
            'default' => null,
            'value' => null
        ),
        'shape' => array(
            'type' => 'string',
            'default' => null,
            'value' => null
        ),
        'title' => array(
            'type' => 'string',
            'default' => null,
            'value' => null
        ),
        'visible' => array(
            'type' => 'bool',
            'default' => true,
            'value' => true
        ),
        'zIndex' => array(
            'type' => 'integer',
            'default' => null,
            'value' => null
        )
    );
    
    public function render(){
        $data = array();
        foreach($this->definitions as $key => $definition){
            if($definition['value'] !== NULL && $definition['default'] !== $definition['value']){
                $data[$key] = $definition['value'];
            }
        }
        $this->code = 'var marker = new google.maps.Marker(' . json_encode($data) . ')';
        return parent::render();
    }
}
