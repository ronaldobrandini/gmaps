<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace core\tools\gmap\options;
/**
 * Description of InfoWindows
 *
 * @author ronaldo.silva
 */
class InfoWindowOptions extends \core\tools\gmap\Output{
    protected $definitions = array(
        'content' => array(
            'type' => 'string',
            'default' => null,
            'value' => null
        ),
        'disableAutoPan' => array(
            'type' => 'bool',
            'default' => true,
            'value' => null
        ),
        'maxWidth' => array(
            'type' => 'number',
            'default' => null,
            'value' => null
        ),
        'pixelOffset' => array(
            'type' => 'object',
            'default' => null,
            'value' => null
        ),
        'position' => array(
            'type' => 'array',
            'default' => null,
            'value' => null
        ),
        'zIndex' => array(
            'type' => 'number',
            'default' => null,
            'value' => null
        )        
    );
    
    public function render(){
        $data = array();
        foreach($this->definitions as $key => $definition){
            if($definition['value'] !== NULL && $definition['default'] !== $definition['value']){
                $data[$key] = $definition['value'];
            }
        }
        $this->code = json_encode($data);
        return parent::render();
    }
}
