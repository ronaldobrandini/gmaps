<?php
namespace core\tools\gmap;

/**
 * Classe utilizada para manipular os mapas do google maps
 *
 * @see https://developers.google.com/maps/documentation/javascript/
 * @author ronaldo.silva <ronaldo.silva@xsystems.com.br>
 */
class Map extends Output{
    /**
     *
     * @var string ID do elemento que receberá o mapa 
     */
    private $mapId;
    /**
     *
     * @var options\MapOptions Objeto de opções do mapa. 
     */
    private $mapOptions;
    /**
     *
     * @var options\PolylineOptions Objeto de opções do Polyline 
     */
    private $polylineOptions;
    /**
     *
     * @var options\PolygonOptions Objeto de opções do Polygon 
     */
    private $polygonOptions;
    /**
     *
     * @var array Lista de objetos de opções do Marker 
     */
    private $markers = array();
    
    public function __construct(options\MapOptions $mapOptions, $mapId = 'map'){
        $this->mapId = $mapId;
        $this->mapOptions = $mapOptions;
    }
    /**
     * 
     * @param \core\tools\gmap\options\PolylineOptions $polylineOptions Objeto de opções do Polyline
     * @return void Nenhum retorno. 
     */
    public function setPolylineOptions(options\PolylineOptions $polylineOptions){
        $this->polylineOptions = $polylineOptions;
    }
    /**
     * 
     * @param \core\tools\gmap\options\MarkerOptions $markerOptions Objeto de opções do Marker
     * @return void Nenhum retorno
     */
    public function setMarkerOptions(options\MarkerOptions $markerOptions){
        $this->markers[] = $markerOptions;
    }
    /**
     * 
     * @param \core\tools\gmap\options\PolygonOptions $polygonOptions Objeto de opções do Polygon
     * @return void Nenhum retorno
     */
    public function setPolygonOptions(options\PolygonOptions $polygonOptions){
        $this->polygonOptions = $polygonOptions;
    }

    /**
     * @param void  Nenhum parametro necessário
     * @return string A string js minificada pronta para usar no html.
     */        
    public function getJs(){
        $this->code = $this->buildCode();
        return $this->code;        
    }
    
    /**
     * Cria o javascript baseado
     *
     * @param void Nenhum parametro necessário
     *
     * @return string
     */
    private function buildCode(){
        $code = '(function(){ ';
        $code .= 'var mapObjmap = document.getElementById(\'' . $this->mapId . '\');';
        $code .= $this->build();        
        $code .= '})(); ';
        return $code;
    }
    
    /**
     * Agrupa todos os objetos numa unica string
     * @param void Nenhum paramentro necessário
     * @return string
     */
    private function build(){
        $arrResult = array();

        if($this->mapOptions){
            $arrResult[] = $this->mapOptions->render();
        }

        $arrResult[] = 'var ' . $this->mapId . ' = new google.maps.Map(mapObjmap, mapOptionsmap);'; 
        
        if($this->polylineOptions){
            $arrResult[] = str_replace('"' . $this->mapId .'"', $this->mapId, $this->polylineOptions->render());
        }
        
        if($this->polygonOptions){
            $arrResult[] = str_replace('"' . $this->mapId .'"', $this->mapId, $this->polygonOptions->render());
        }
        
        if($this->markers){
            foreach($this->markers as $marker){
                $arrResult[] = str_replace('"' . $this->mapId .'"', $this->mapId, $marker->render());
            }
        }
        
        return implode(';', $arrResult);
    }
}
