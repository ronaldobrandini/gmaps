<?php
namespace core\tools\gmap\services;
/**
 * Description of Geocoder
 *
 * @author ronaldo.silva
 */
class Geocode extends Service{
    protected $service = 'geocode';
    
    public function __construct($address, $sensor = 'false'){
        $this->setParam('address', $address);
        $this->setParam('sensor', $sensor);
    }
}
