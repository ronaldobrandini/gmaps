<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace core\tools\gmap\services;

/**
 * Description of Service
 *
 * @author ronaldo.silva
 */
class Service{
    const BASE_URL = 'http://maps.googleapis.com/maps/api/';
    protected $responseType = 'json';
    protected $params = array();

    
    protected function request(){
        $url = self::BASE_URL . $this->service . '/' . $this->responseType . '?' . http_build_query($this->params);
        $cookie = tempnam ("/tmp", "CURLCOOKIE");
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $ch, CURLOPT_ENCODING, "" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
        curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
        $content = curl_exec( $ch );
        $response = curl_getinfo( $ch );
        curl_close ( $ch );
        if($response['http_code'] == 200){
            //echo '<pre>' . $content . '</pre>';
           return $content;
        }else{
            return false;
        }
    }
    
    public function setParam($name, $value){
        $this->params[$name] = $value;
    }
    
    public function execute(){
        $response = $this->request();
        if($response){
            $obj = json_decode($response);
            if(is_object($obj)){
                return $obj;
            }
        }
        
        return false;
    }
}
