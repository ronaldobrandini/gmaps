<?php

namespace core\tools\gmap\services;

/**
 * Description of Distance
 *
 * @author ronaldo.silva
 */
class Distance extends Service{
    protected $service = 'distancematrix';
    
    public function __construct($origins, $destinations, $sensor = 'true'){
        $this->setParam('origins', $origins);
        $this->setParam('destinations', $destinations);
        $this->setParam('sensor', $sensor);
    }
    
       
    
}
