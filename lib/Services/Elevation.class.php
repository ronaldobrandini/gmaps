<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace core\tools\gmap\services;

/**
 * Description of Elevation
 *
 * @author ronaldo.silva
 */
class Elevation extends Service{
    protected $service = 'elevation';
    
    public function __construct($locations, $path = null, $samples = null, $sensor = 'true'){
        $this->setParam('locations', $locations);
        if($path){
            $this->setParam('path', $path);
        }
        if($samples){
            $this->setParam('samples', $samples);
        }
        $this->setParam('sensor', $sensor);
    }
    
}
