<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace core\tools\gmap\services;

/**
 * Description of Diretions
 *
 * @author ronaldo.silva
 */
class Directions extends Service{
    protected $service = 'directions';
    
    public function __construct($origin, $destination, $sensor = 'false'){
        $this->setParam('origin', $origin);
        $this->setParam('destination', $destination);
        $this->setParam('sensor', $sensor);
    }
    
    
}
