<?php

namespace Exception;

/**
 * Description of GmapDefinitionDoNotExistsException
 *
 * @author desenvolvimento
 */
class GmapDefinitionDoNotExistsException extends \Exception
{
}
